package com.example.myapplication

import android.util.Patterns
import java.util.regex.Matcher
import java.util.regex.Pattern


public class Validation {

    fun isValidPassword(pass: String?): Boolean {

        val pattern: Pattern
        val matcher: Matcher

        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$"

        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(pass)

        return matcher.matches()

    }

    fun isEmailValid(email: String?): Boolean {
        var isValid = false

        val pattern = Pattern.compile(Patterns.EMAIL_ADDRESS.pattern(), Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        if (matcher.matches()) {
            isValid = true
        }

        return isValid
    }

}