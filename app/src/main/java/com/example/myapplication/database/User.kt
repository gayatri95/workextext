package com.example.myapplication.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    var name: String,
    var email: String,
    var mobile: String,
    var detail: String
) {



    override fun toString(): String {
        return "User(id=$id, name=$name, email=$email, mobile=$mobile, detail=$detail)"
    }


}