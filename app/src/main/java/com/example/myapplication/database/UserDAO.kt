package com.example.myapplication.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query


@Dao
interface UserDAO {

    @Insert
    fun insert(user: User)

    @Query("SELECT * FROM User " + "User")
    fun getUser(): User

}