package com.example.myapplication.view

interface LoginView {

    fun onLoginResult(result: Boolean)

}
