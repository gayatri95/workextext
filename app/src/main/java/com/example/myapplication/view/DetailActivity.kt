package com.example.myapplication.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.database.User
import com.example.myapplication.database.UserDatabase
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity(), View.OnClickListener {


    private var db: UserDatabase? = null
    private var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        button_back?.setOnClickListener(this)

        db = UserDatabase.getDatabase(this)

        Log.println(Log.ASSERT, "USer", db?.userDao()?.getUser().toString())

        user = db?.userDao()?.getUser()

        tv_user_name?.text = user?.name
        tv_user_details?.text = user?.detail
        tv_user_email?.text = user?.email
        tv_user_mobile?.text = user?.mobile

    }

    override fun onClick(p0: View?) {

        when (p0?.id) {

            R.id.button_back -> {

                db?.clearAllTables()

                Log.println(Log.ASSERT, "USer", db?.userDao()?.getUser().toString())


                val myIntent = Intent(this, MainActivity::class.java)
                this.startActivity(myIntent)

            }
        }
    }

    @SuppressLint("WrongThread")
    override fun onBackPressed() {
        db?.clearAllTables()
        val myIntent = Intent(this, MainActivity::class.java)
        this.startActivity(myIntent)
    }
}
