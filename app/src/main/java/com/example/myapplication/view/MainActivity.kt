package com.example.myapplication.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.Validation
import com.example.myapplication.database.User
import com.example.myapplication.database.UserDatabase
import com.example.myapplication.presenter.LoginPresenter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), View.OnClickListener, LoginView {

    var loginPresenter: LoginPresenter? = null
    private var db: UserDatabase? = null
    private val validation = Validation()


    @SuppressLint("WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_signup?.setOnClickListener(this)

        loginPresenter = LoginPresenter(this)

        db = UserDatabase.getDatabase(this)


    }

    override fun onClick(v: View?) {

        when (v?.id) {

            R.id.btn_signup -> {

                if (isScreenValid()) {
                    loginPresenter?.doLogin(
                        et_name?.text?.toString(),
                        et_password?.text?.toString()
                    )
                }
            }
        }
    }

    private fun isScreenValid(): Boolean {

        if (et_name?.text?.isEmpty()!!) {
            Toast.makeText(this, "Please enter username", Toast.LENGTH_SHORT).show()
            return false
        } else if (et_password?.text?.isEmpty()!!) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show()
            return false
        } else if (et_password?.text?.length!! < 6) {
            Toast.makeText(this, "Please enter 6 digit password", Toast.LENGTH_SHORT).show()
            return false
        } else if (!validation.isValidPassword(et_password?.text?.toString())) {
            Toast.makeText(
                this,
                "Password must contain at least one number and one special char",
                Toast.LENGTH_SHORT
            ).show()
            return false
        } else if (et_email?.text?.isEmpty()!!) {
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show()
            return false
        } else if (!validation.isEmailValid(et_email?.text?.toString())) {
            Toast.makeText(this, "Please enter valid email", Toast.LENGTH_SHORT).show()
            return false
        } else if (et_mobile?.text?.isEmpty()!!) {
            Toast.makeText(this, "Please enter mobile number", Toast.LENGTH_SHORT).show()
            return false
        } else if (et_mobile.text!!.length < 10) {
            Toast.makeText(this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }

    private fun openActivity() {
        val myIntent = Intent(this, DetailActivity::class.java)
        this.startActivity(myIntent)
    }

    override fun onLoginResult(result: Boolean) {

        if (result) {
            db?.clearAllTables()
            Toast.makeText(this, "Login Success", Toast.LENGTH_SHORT).show()
            val user = User(
                name = et_name?.text.toString(),
                email = et_email?.text.toString(),
                mobile = et_mobile?.text.toString(),
                detail = et_about_me?.text.toString()
            )

            db?.userDao()?.insert(user)

            openActivity()

            Log.println(Log.ASSERT, "USer", db?.userDao()?.getUser().toString())

        } else
            Toast.makeText(this, "Login Fail", Toast.LENGTH_SHORT).show()

    }
}



