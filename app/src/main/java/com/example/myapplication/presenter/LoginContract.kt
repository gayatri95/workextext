package com.example.myapplication.presenter

interface LoginContract {

    fun clear()
    fun doLogin(name: String?, password: String?)

}