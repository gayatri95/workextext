package com.example.myapplication.presenter

import com.example.myapplication.model.UserDetail
import com.example.myapplication.view.LoginView

class LoginPresenter(view: LoginView) : LoginContract {

    var loginView = view
    var userDetail: UserDetail = UserDetail()


    override fun clear() {

    }

    override fun doLogin(name: String?, password: String?) {

        if (userDetail?.isValidPassword(name, password)!!) {
            loginView.onLoginResult(true)
        } else {
            loginView.onLoginResult(false)
        }

    }
}